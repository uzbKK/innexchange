app.controller 'DepositsController', ['$scope', '$stateParams', '$http', '$filter', '$gon', 'md5', ($scope, $stateParams, $http, $filter, $gon, md5) ->
  @deposit = {}
  $scope.currency = $stateParams.currency
  $scope.currencyTranslationLocals = currency: $stateParams.currency.toUpperCase()
  $scope.current_user = current_user = $gon.user
  $scope.name = current_user.name
  $scope.bank_details_html = $gon.bank_details_html
  $scope.account = Account.findBy('currency', $scope.currency)
  $scope.currencyType = if _.include(gon.fiat_currencies, $scope.currency) then 'fiat' else 'coin'
  $scope.data = amount: ''

  $scope.$watch (-> $scope.account.deposit_address), ->
    setTimeout(->
      $.publish 'deposit_address:create'
    , 1000)

  $scope.$watch (-> $scope.data.amount), ->
      if($('input[name="PAYEE_ACCOUNT"]').length)
          units = $('input[name="PAYMENT_UNITS"]').val()
          apiKey = $gon.epay_api_key
          $scope.data.epayEmail = $gon.epay_api_email
          data = $scope.data.epayEmail+':'+$scope.data.amount+':'+units+':'+apiKey
          $scope.data.md5hash = md5.createHash(data or '')

]
