#= require yarn_components/raven-js/dist/raven
#= require ./lib/sentry

#= require es5-shim.min
#= require es5-sham.min
#= require jquery
#= require jquery_ujs
#= require jquery-timing.min
#= require popper
#= require bootstrap
#= require scrollIt
#= require moment
#= require bignumber
#= require underscore
#= require clipboard
#= require flight.min
#= require pusher.min
#= require list
#= require jquery.mousewheel
#= require jquery-timing.min
#= require qrcode
#= require cookies.min

#= require ./lib/pusher_connection
#= require ./lib/tiny-pubsub

#= require highstock
#= require_tree ./helpers

$ ->
  BigNumber.config(ERRORS: false)

  $('[data-clipboard-text], [data-clipboard-target]').each ->
      clipboard = new Clipboard(this)

  if $('#assets-index').length
    $.scrollIt
      topOffset: -180
      activeClass: 'active'

    $('a.go-verify').on 'click', (e) ->
      e.preventDefault()

      root         = $('.tab-pane.active .root.json pre').text()
      partial_tree = $('.tab-pane.active .partial-tree.json pre').text()

      if partial_tree
        uri = 'http://syskall.com/proof-of-liabilities/#verify?partial_tree=' + partial_tree + '&expected_root=' + root
        window.open(encodeURI(uri), '_blank')

  $('.qrcode-container').each (index, el) ->
    $el = $(el)
    new QRCode el,
      text:   $el.data('text')
      width:  $el.data('width')
      height: $el.data('height')

  $(".menu_btn").click ->
      $menu = $(".main_menu_wraper:not(.mobile)")
      if $menu.is(':visible')
          $menu.hide()
      else
          $menu.show()
  if $("#main_page_markets_table").length > 0
      output = ''
      for k,element of gon.tickers
          p1 = parseFloat(element.open)
          p2 = parseFloat(element.last)
          per = formatter.round(100*(p2-p1)/p1, 2)
          sign = ''
          if p1>p2
              color = 'red'
          else if p1<p2
              color = 'green'
              sign = '+'
          output +='<tr>
              <td>'+element.name+'</td>
              <td>'+element.last+' / $ 1,189.34</td>
              <td class="'+color+'">'+sign+''+per+' %</td>
              <td>'+element.high+'</td>
              <td>'+element.low+'</td>
              <td>'+element.volume+' '+element.base_unit.toUpperCase()+'</td>
          </tr>'
      $("#main_page_markets_table").html(output)
      # console.log gon
