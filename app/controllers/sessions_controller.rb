# Rails.logger = logger = Logger.new("#{Rails.root}/log/my.log")
class SessionsController < ApplicationController
  before_action :auth_member!, only: :destroy
  before_action :auth_anybody!, only: [:new, :failure, :login, :regist]

  skip_before_filter :check_mfa

  layout 'sessions'

  def new
    @member = Member.new
  end

  # def my_logger
  #   @@my_logger ||= Logger.new("#{Rails.root}/log/my.log")
  # end

  def create
    # my_logger.info(auth_hash.inspect)
    if auth_hash
        @member = Member.from_auth(auth_hash)
    else
        if params[:'g-recaptcha-response'].present?
            uri = URI.parse("https://www.google.com/recaptcha/api/siteverify")
            response = Net::HTTP.post_form(uri, {"secret" => ENV['RECAPTCHA_SECRET'], "response" => params[:'g-recaptcha-response']})
            respObj = JSON.parse response.body
            if respObj['success']
                # Rails.logger.info params[:member].inspect
                # site auth
                @member = Member.authenticate(params[:member][:email],params[:member][:password])
            else
                return redirect_on_unsuccessful_recaptcha
            end
        else
            return redirect_on_unsuccessful_recaptcha
        end
    end
        return redirect_on_unsuccessful_sign_in unless @member
        return redirect_to(root_path, alert: t('.disabled')) if @member.disabled?

        reset_session rescue nil
        session[:member_id] = @member.id
        memoize_member_session_id @member.id, session.id

        if @member.two_factor_auth_status == 1
            redirect_to new_member_mfa_session_path
        else
            redirect_on_successful_sign_in
        end
  end

  def login
    @member = Member.new
  end

  def regist
    @member = Member.new
    # MemberMailer.test_email('2ovob4ehko@gmail.com').deliver_now
  end

  def sign_up
      @member = Member.new(member_params)
      if params[:'g-recaptcha-response'].present?

          uri = URI.parse("https://www.google.com/recaptcha/api/siteverify")
          response = Net::HTTP.post_form(uri, {"secret" => ENV['RECAPTCHA_SECRET'], "response" => params[:'g-recaptcha-response']})
          respObj = JSON.parse response.body
          if respObj['success']
              if @member.save
                  flash.now[:notice] = %Q[You signed up successfully. <a href="/login">Login</a>].html_safe
                  # Rails.logger.info     MemberMailer.methods
                  MemberMailer.verif_email(@member).deliver_now
              else
                  flash.now[:alert] = "Form is invalid"
              end
          else
              flash.now[:alert] = "Google recaptcha was not checked"
          end
      else
          flash.now[:alert] = "Google recaptcha was not checked"
      end
      render "regist"
  end

  def forgot

  end

  def send_recover_pass
      if params[:'email'].present?
          @member = Member.find_by email: params[:'email']
          if @member
              MemberMailer.forgot_email(@member).deliver_now
              return redirect_to forgot_path, notice: "A password recovery letter was sent to your email"
          else
              return redirect_to forgot_path, alert: "User with this email not exist"
          end
      else
          return redirect_to forgot_path, alert: "Email field is empty"
      end

  end

  def recover_pass
      @member = Member.find_by salt: params[:salt]
      if @member

      else
          return redirect_to login_path, alert: "Password recovery link is not correct"
      end
  end

  def reset_pass
      if params[:member][:password] == params[:member][:password_confirmation]
          @member = Member.find_by salt: params[:salt]
          if @member
              @member.salt = BCrypt::Engine.generate_salt
              @member.encrypted_password = BCrypt::Engine.hash_secret(params[:member][:password], @member.salt)
              @member.save!
              return redirect_to login_path, notice: "Password was changed successfully"
          else
              return redirect_to login_path, alert: "Password recovery link is not correct"
          end
      else
          return redirect_to recover_pass_path(params[:salt]), alert: "Confirmation password does not match"
      end
  end

  def change_mfa_status
      @member = current_user
      if params[:two_factor_auth_status].nil?
          @member.two_factor_auth_status = 0
      else
          @member.two_factor_auth_status = 1
          @member.set_google_secret
          MemberMfaSession.create(@member)
      end
      @member.save
      redirect_to settings_path
  end

  def privacy_agree
      @member = current_user
      @member.privacy = 1
      @member.save
      redirect_to root_path
  end

  def verif_email
      member = Member.find_by salt: params[:salt]
      # Rails.logger.info params[:salt].inspect
      # Rails.logger.info member.inspect
      return redirect_on_unsuccessful_sign_in unless member
      member.varification = 1
      member.save!
      redirect_to root_path
  end

  def failure
    redirect_to root_path, alert: t('.error')
  end

  def destroy
    destroy_member_sessions(current_user.id)
    MemberMfaSession.destroy
    reset_session
    redirect_to root_path
  end

private

  def auth_hash
    @auth_hash ||= request.env['omniauth.auth']
  end

  def member_params
    params.require(:member).permit(:username, :email, :password, :password_confirmation)
  end

  def redirect_on_successful_sign_in
    "#{params[:provider].to_s.gsub(/(?:_|oauth2)+\z/i, '').upcase}_OAUTH2_REDIRECT_URL".tap do |key|
      if ENV[key] && params[:provider].to_s == 'barong'
        redirect_to "#{ENV[key]}?#{auth_hash.fetch('credentials').to_query}"
      elsif ENV[key]
        redirect_to ENV[key]
      else
        redirect_back_or_settings_page
      end
    end
  end

  def redirect_on_unsuccessful_sign_in
    redirect_to login_path, alert: t('.error')
  end

  def redirect_on_unsuccessful_recaptcha
    redirect_to login_path, alert: 'Google recaptcha was not checked'
  end
end
