#  Rails.logger = logger = Logger.new("#{Rails.root}/log/my.log")
class MemberMfaSessionsController < ApplicationController
  skip_before_filter :check_mfa

  def new
    # Rails.logger.info current_user.inspect
    @member = current_user
  end

  def create
    @member = current_user
    if @member.google_authentic?(params[:auth][:mfa_code])
      MemberMfaSession.create(@member)
      redirect_to root_path
    else
      flash[:error] = "Wrong code"
      render :new
    end
  end
end
