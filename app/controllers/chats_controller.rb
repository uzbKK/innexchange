class ChatsController < ApplicationController
    def show
        @chats = Chat.all
        render layout: false
    end
    def create
        cp = chat_params
        cp[:member_id] = current_user.id
        @chat = Chat.new(cp)
        @chat.save
        @chats = Chat.all
        render 'show', layout: false
    end

    private
    def chat_params
        params.require(:chat).permit(:member_id, :message)
    end
end
