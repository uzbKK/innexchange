Rails.logger = logger = Logger.new("#{Rails.root}/log/my.log")
class EpayController < ApplicationController
    def paystatus
      # it is just coding. I don't know ruby and rail too. EPay want return "Success"
      render plain: "Success"
    end

    def payment
      # it is just coding. I don't know ruby and rail too.
      # TODO: get member sn from EPay and sum
      # increase member account
      # show success page
      sum = params['PAYMENT_AMOUNT'] # 100
      fiat = params['PAYMENT_UNITS'] # 'EUR'
      member_sn = params['PAYER_ACCOUNT'] # 'SN2F4541BACA'

      Rails.logger.info params['PAYER_ACCOUNT']
      # sum = 100
      # fiat = 'EUR'
      # member_sn = 'SN2F4541BACA'

      member = Member.where(sn: member_sn).take
      currency = Currency.where(code: fiat.downcase).take
      account = Account.where(member_id: member.id, currency: currency.id).take
      account.increment(:balance, sum)
      account.save
      redirect_to funds_path
    end

    def nopayment
      # it is just coding. I don't know ruby and rail too.
      # TODO: show failure page
      render "nopayment"
    end
end
