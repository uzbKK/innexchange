class ApplicationMailer < ActionMailer::Base
  default from: "admin@coinagewallet.com",
          reply_to: "admin@coinagewallet.com"
  layout 'mailer'
end
