class MemberMailer < ApplicationMailer
    default from: 'CoinageWallet <admin@coinagewallet.com>',
            reply_to: "admin@coinagewallet.com"

    def verif_email(member)
        @member = member
        mail(to: @member.email, subject: 'Email varification')
    end

    def test_email(email)
        mail(to: email, subject: 'Test email')
    end

    def forgot_email(member)
        @member = member
        mail(to: @member.email, subject: 'Password recovery')
    end
end
