class Chat < ActiveRecord::Base
    belongs_to :member

    def to_s
        "#{member.username}: #{message}"
    end
end

# == Schema Information
# Schema version: 20180709121306
#
# Table name: chats
#
#  id         :integer          not null, primary key
#  member_id  :integer
#  message    :string(255)
#  created_at :datetime
#  updated_at :datetime
#
