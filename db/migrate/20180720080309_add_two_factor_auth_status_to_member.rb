class AddTwoFactorAuthStatusToMember < ActiveRecord::Migration
  def change
      add_column :members, :two_factor_auth_status, :integer, limit: 1, default: 0
  end
end
