class AddPrivacyToMember < ActiveRecord::Migration
  def change
      add_column :members, :privacy, :integer, limit: 1, default: 0
  end
end
