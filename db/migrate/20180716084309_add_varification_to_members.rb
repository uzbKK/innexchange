class AddVarificationToMembers < ActiveRecord::Migration
  def change
      add_column :members, :varification, :integer, limit: 1, default: 0
  end
end
