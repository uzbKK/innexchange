class AddPasswordToMembers < ActiveRecord::Migration
  def change
      add_column :members, :username, :string
      add_column :members, :encrypted_password, :string
      add_column :members, :salt, :string
  end
end
