class UserMailerPreview < ActionMailer::Preview
  def varif_email
    UserMailer.with(member: Member.first).verif_email
  end
end
